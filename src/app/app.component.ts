import {Component, ViewChild} from "@angular/core";
import {Platform, Nav, AlertController} from "ionic-angular";
import {StatusBar, Push, Splashscreen} from "ionic-native";
import { LoginPage } from '../pages/login/login';
import { Storage } from '@ionic/storage';

@Component({
  template: `<ion-nav [root]="rootPage"></ion-nav>`
})
export class MyApp {
  // the root nav is a child of the root app component
  // @ViewChild(Nav) gets a reference to the app's root nav
  @ViewChild(Nav) nav: Nav;
  rootPage = LoginPage;
  messageid: string;

  constructor(public platform: Platform,
              public alertCtrl: AlertController,
              public storage: Storage) {

    platform.ready().then(() => {
      /*enableProdMode();
        this.storage.ready().then(() => {
        this.storage.clear();
      });*/
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
      this.initPushNotification();
    });
  }

  initPushNotification(){
    if (!this.platform.is('cordova')) {
      console.warn("Push notifications not initialized. Cordova is not available - Run in physical device");
      return;
    }
    let push = Push.init({
      android: {
        senderID: "170062317604"
      },
      ios: {
        alert: "true",
        badge: false,
        sound: "true"
      },
      windows: {}
    });

    push.on('registration', (data) => {
      this.storage.ready().then(() => {
        this.storage.set('pushtoken', data.registrationId);
      });
    });

    push.on('notification', (data) => {

        let self = this;

        //if user using app and push notification comes
        if (data.additionalData.foreground) {

          // if application is open, show popup
          let confirmAlert = this.alertCtrl.create({
            title: 'Fingerlog Login Request',
            message: data.message,
            buttons: [{
              text: 'Ignore',
              role: 'cancel'
            }, {
              text: 'View',
              handler: () => {
                self.nav.push(LoginPage, {messageid: data.additionalData.messageid});
              }
            }]
          });

          confirmAlert.present();

        } else {

          //if user NOT using app and push notification comes

          this.storage.ready().then(() => {
            this.storage.set('messageid', data.additionalData.messageid);
            console.log("---- THE MESSAGE ID STORED FOR FINGERLOG: ---- " + data.additionalData.messageid);
          });

          self.nav.push(LoginPage, {messageid: data.additionalData.messageid});
          console.log("Push notification clicked");
        }

    });
    push.on('error', (e) => {
      console.log(e.message);
    });
  }
}
