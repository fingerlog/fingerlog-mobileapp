import {Injectable} from '@angular/core';
import {Device} from 'ionic-native';
import {Platform} from 'ionic-angular';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {Http, Headers, RequestOptions} from '@angular/http';


export class User {
  name: string;
  email: string;
  authenticated: boolean;

  constructor(name: string, email: string) {
    this.name = name;
    this.email = email;
    this.authenticated = true;
  }
}

@Injectable()
export class AuthService {
currentUser: User;
apiURL: string;
connection: Http;
data: [string, string];
loggedUserInfo: Array<any>;
deviceID: string;
status: any;
platf: Platform;
userEmail: string;
userName: string;


constructor(private http: Http, platform: Platform) {
  this.data = ["response", ""];
  this.connection = http;
  this.apiURL = 'http://dulak.info/fingerlog/public/api/auth/';
  this.platf = platform;
  this.platf.ready().then(() => {
    this.deviceID = Device.uuid;
    this.getUserEmail().then((result) => {
      this.userEmail = result['email'];
      this.userName = result['username'];
    }, err => {
      console.log('Could not get user email');
    }).catch((err) => {
      console.log('Could not get user email problem');
    });
  });
}

public login(password) {
  return new Promise((resolve, reject) => {
    if (this.userEmail === null || password === null) {
      reject("Username or password cannt be empty");
    } else {
        console.log("Attempting to authenticate for: " + this.userEmail);
        console.log("Using password: " + password);
        this.authenticateUser(this.userEmail, password).subscribe(result => {
          this.currentUser = new User("User", this.userEmail);
          console.log(result);
          resolve(true);
        }, (err) => {
          reject("Wrong username or password");
        });
    }
  });
}

public register(credentials, pushtoken, password)
{
  return new Promise((resolve, reject) => {
    if (credentials.email === null || password === null) {
      reject("Username or password cannt be empty");
    } else {
        this.registerUser(credentials, pushtoken, password).subscribe(result => {
          this.currentUser = new User(credentials.firstname, credentials.email);
          resolve(true);
        }, (err) => {
          reject("Email already in used or wrong format.");
        });
    }
  });
}

public deviceExists() : any
{
  return new Promise((resolve, reject) => {
    if (this.deviceID == null) {
      reject("Device ID is null");
    } else {
        this.isLoggedIn().subscribe(result => {
          resolve(result);
        }, (err) => {
          reject(err);
        });
    }
  }).catch(err => {
    return "error";
  });
}

isLoggedIn()
{
      let url = `${this.apiURL}getdevice`;
      let params = `deviceid=${this.deviceID}`;
      let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded'});
      let options = new RequestOptions({ headers: headers });
      return this.http.post(url, params, options)
        .map(res => { return res.json(); })
        .catch(res => { return res.json(); });
}

authenticateUser(email, password)
{
      let url = `${this.apiURL}login`;
      let params = `email=${email}&password=${password}`;
      let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded'});
      let options = new RequestOptions({ headers: headers });
      return this.http.post(url, params, options)
        .map(res => { this.data = ["response", res.json()]; return this.data; })
        .catch(res => { return res.json(); })
}

public giveAccess(messageid) : any
{
    return new Promise((resolve, reject) => {
      if (messageid == null) {
        reject("Message id is null");
      } else {
          this.updateAccess(messageid).subscribe(result => {
            resolve(result);
          }, (err) => {
            reject(err);
          });
      }
    }).catch(err => {
      return "[catch error]";
    });
}

  updateAccess(messageid)
  {
    let url = `${this.apiURL}updateaccess`;
    let params = `messageid=${messageid}&deviceid=${this.deviceID}`;
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded'});
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, params, options)
      .map(res => { this.data = ["response", res.json()]; return this.data; })
      .catch(res => { return res.json(); })
  }

  registerUser(credentials, pushtoken, password)
  {
      let url = `${this.apiURL}signup`;
      let params = `email=${credentials.email}&password=${password}&deviceid=${this.deviceID}&name=${credentials.firstname}&pushtoken=${pushtoken}`;
      let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded'});
      let options = new RequestOptions({ headers: headers });
      return this.http.post(url, params, options)
        .map(res => { this.data = ["response", res.json()]; return this.data; })
        .catch(res => { return res.json(); })
  }

  getUserEmail()
  {
    return new Promise((resolve, reject) => {
      if (this.deviceID == null) {
        reject("Device id is null");
      } else {
        this.getEmail().subscribe(result => {
          resolve(result);
        }, (err) => {
          reject(err);
        });
      }
    });
  }

  getEmail()
  {
    let url = `${this.apiURL}getemail`;
    let params = `deviceid=${this.deviceID}`;
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded'});
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, params, options)
      .map(res => { return res.json(); })
      .catch(res => { return res.json(); })
  }

  public getUserInfo() : User
  {
    return this.currentUser;
  }
}
