import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, Loading } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { RegisterPage } from '../register/register';
import {Platform} from 'ionic-angular';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';
import {Device} from 'ionic-native';

declare var FingerprintAuth: any;

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  loading: Loading;
  registerCredentials = {email: '', password: ''};
  deviceStatus : any;
  platf: Platform;
  messageid = "empty";
  userEmail: string;
  userName: string;
  fingerlogtoken: string;

  constructor(private nav: NavController, private auth: AuthService, private alertCtrl: AlertController, private loadingCtrl: LoadingController, platform: Platform, public storage: Storage) {
    this.platf = platform;

    this.platf.ready().then(() => {
      this.storage.ready().then(() => {

        this.auth.getUserEmail().then((result) => {
          this.userEmail = result['email'];
          this.userName = result['username'];
        }, err => {
          console.log('Could not get user email');
        }).catch((err) => {
          console.log('Could not get user email problem');
        });

        this.storage.get('messageid').then((data) => {
          if (data != null) {
            this.messageid = data;
          }
        });

        this.storage.get('fingerlogtoken').then((data) => {
          if (data != null) {
            this.fingerlogtoken = data;
          }
        });

      });
    });

  }

  ionViewDidLoad() {
    this.platf.ready().then(() => {

      this.showLoading();

      this.auth.deviceExists().then(result => {
        if(result['status'] === 'ok') {
          this.loading.dismiss();
        } else {
          setTimeout(() => {
            this.loading.dismiss();
            this.nav.setRoot(RegisterPage);
          });
        }
      }, err => {
        setTimeout(() => {
          this.loading.dismiss();
          this.nav.setRoot(RegisterPage);
        });
      }).catch((err) => {
        this.loading.dismiss();
        this.nav.setRoot(RegisterPage);
      });
    });
  }

  public createAccount() {
    this.nav.push(RegisterPage);
  }


  login()
  {
      this.showLoading();
      this.fingerlogCheck().then((passString:string) => {
        if(passString != "error") {
          if(passString != "empty") {
            this.auth.login(passString).then(result => {
              setTimeout(() => {
                this.loading.dismiss();
                this.auth.giveAccess(this.messageid).then(result => {
                  console.log('authentication for :');
                  console.log(this.messageid);
                }, err => {
                  console.log('message id = problem');
                }).catch((err) => {
                  console.log('message id = error');
                });
                this.nav.setRoot(HomePage);
              });
            }).catch((err) => {
              this.loading.dismiss();
              this.showError(err);
            });
          } else {
            this.loading.dismiss();
            this.showError("Fingerlog was not used.");
          }
        } else {
          this.loading.dismiss();
          this.showError("pass string = error");
        }
      }).catch((err) => {
        this.loading.dismiss();
        this.showError(err);
      });
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  showError(text) {
    setTimeout(() => {
      this.loading.dismiss();
    });

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  fingerlogCheck() {

    var decryptConfig = {
      clientId: "fingerlog",
      username: Device.uuid,
      token: this.fingerlogtoken,
      dialogTitle: "Fingerlog Login",
      dialogMessage: "Please confirm your identity using a Fingerprint Scanner.",
      dialogHint: "You can add more fingerprints to your local android Fingerprint Manager."
    };

    return new Promise((resolve, reject) => {
      FingerprintAuth.decrypt(decryptConfig, successCallback, errorCallback);

      function successCallback(result) {
          if (result.withFingerprint) {
              console.log("Successful biometric authentication.");
              if (result.password) {
                  console.log("Successfully decrypted credential token.");
                  resolve(result.password);
              }
          }
      }

      function errorCallback(error) {
        console.log("FingerprintAuth Error: " + error);
        reject("Fingerprint error " + error);
      }
    });
  }
}
