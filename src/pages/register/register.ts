import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, Loading, Platform } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';
import {Device} from 'ionic-native';

declare var FingerprintAuth: any;

@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {
  loading: Loading;
  registerCredentials = {email: '', name: ''};
  pushtoken: string;
  usercredentials: string;
  fingerlogtoken: string;
  userPassword: string;

  constructor(private nav: NavController, private auth: AuthService, private alertCtrl: AlertController, private loadingCtrl: LoadingController, public storage: Storage, public platf: Platform) {

    // Generate unique password
    this.platf.ready().then(() => {
      this.storage.ready().then(() => {
        this.storage.get('pushtoken').then((val) => {
          this.pushtoken = val;
          console.log("PUSH TOKEN IN REGISTER CONSTRUCTOR : " + val);
        });
      });
    });
  }

  public register() {
    this.showLoading();

    var randomString = function(length) {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      for(var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }
      return text;
    }

    this.userPassword = randomString(16);

    console.log("Random password -> " + this.userPassword);

    this.fingerlogCheck().then((passString:string) => {
      if(passString != "error") {

        this.fingerlogtoken = passString;
        console.log("FINGERLOG FINGERPRINT RETURNED THE FOLLOWING STRING -> " + passString);

        if(this.fingerlogtoken != "empty") {

          this.platf.ready().then(() => {
            this.storage.ready().then(() => {
              this.storage.set('fingerlogtoken', this.fingerlogtoken);
            });
          });

          this.auth.register(this.registerCredentials, this.pushtoken, this.userPassword).then(result => {
            setTimeout(() => {
              this.loading.dismiss();
              this.nav.setRoot(HomePage);
            });
          }).catch((err) => {
            this.loading.dismiss();
            this.showError(err);
          });
        } else {
          this.loading.dismiss();
          this.showError("Fingerlog was not used.");
        }
      } else {
        this.loading.dismiss();
        this.showError("pass string = error");
      }
    }).catch((err) => {
      this.loading.dismiss();
      this.showError(err);
    });
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  showError(text) {
    setTimeout(() => {
      this.loading.dismiss();
    });

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  fingerlogCheck() {
    var tempPass = this.userPassword;
    return new Promise((resolve, reject) => {
      FingerprintAuth.isAvailable(function (result) {

        console.log("FingerprintAuth available: " + JSON.stringify(result));
          // If has fingerprint device and has fingerprints registered
          if (result.isAvailable == true) {
            if(result.hasEnrolledFingerprints == true) {

              // Check the docs to know more about the encryptConfig object :)
              var encryptConfig = {
                clientId: "fingerlog",
                username: Device.uuid,
                password: tempPass,
                maxAttempts: 5,
                disableBackup: true,
                dialogTitle: "Fingerlog Registration",
                dialogMessage: "Please confirm your identity using a Fingerprint Scanner.",
                dialogHint: "You can add more fingerprints to your local android Fingerprint Manager."
              };

              console.log("Fingerlog Fingerprint preEncrypted Config: " + JSON.stringify(encryptConfig));

              // Set config and success callback
              FingerprintAuth.encrypt(encryptConfig, function(_fingerResult) {
                console.log("successCallback(): " + JSON.stringify(_fingerResult));
                if (_fingerResult.withFingerprint) {
                  resolve(_fingerResult.token);
                }

              // Error callback
              }, function(err) {
                if (err === "Cancelled") {
                  reject("Error");
                } else {
                  reject("Error");
                }
              });
          } else {
            // Request user to add fingerprints to fingerprint manager
            reject("Error");
          }
        } else {
          reject("Error");
        }

      }, function (message) {
        console.log("isAvailableError(): " + message);
        reject("Error");
      });
    });
  }
}
