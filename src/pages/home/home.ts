import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {AuthService} from '../../providers/auth-service';
import { Storage } from '@ionic/storage';
import {Platform} from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  name = '';
  email = '';
  password = '';
  messageid = "empty";

  constructor(public nav: NavController, private auth: AuthService, public storage: Storage, platform: Platform) {

  this.platf.ready().then(() => {
    this.storage.ready().then(() => {

    this.storage.get('messageid').then((data) => {
      console.log('Messageid (MESSAGE ID) in Home Page', data)
      if (data != null) {
        this.messageid = data;
      }
      console.log("---- THE MESSAGE ID STORED FOR FINGERLOG (in Home Page): ---- " + this.messageid);
    });

    this.auth.getUserEmail().then((result) => {
      this.email = result['email'];
      this.name = result['username'];
    }, err => {
      console.log('Could not get user email');
    }).catch((err) => {
      console.log('Could not get user email problem');
    });
  }
}
